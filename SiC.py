#! /usr/bin/env python

import math
import os
import sys
import numpy as np
from BigDFT import Logfiles as lf 
import constants as const


# extract data from the logfiles
def get_data(seed, calc_type, num_units, smear, label, noise, get_dos=True, get_pos=True, get_error=True, get_cubic=True):

    get_forces = False

    data = {'label': label}
    filenames=list()
    filenames_cubic=list()

    if noise == '0':
        filename = "log-"+str(seed)+"_"+str(num_units)+"_"+str(calc_type)+".yaml"
        filename_cubic = "log-"+str(seed)+"_"+str(num_units)+"_cubic.yaml"
        filename_linear = "log-"+str(seed)+"_"+str(num_units)+"_linear.yaml"
    else:
        filename = "log-"+str(seed)+"_"+str(num_units)+"_"+noise+"_"+str(calc_type)+".yaml"
        filename_cubic = "log-"+str(seed)+"_"+str(num_units)+"_"+noise+"_cubic.yaml"
        filename_linear = "log-"+str(seed)+"_"+str(num_units)+"_"+noise+"_linear.yaml"
        filename_ref = "log-"+str(seed)+"_"+str(num_units)+"_"+str(calc_type)+".yaml"

    if (not os.path.exists(filename)):
        print "Warning: missing logfile "+str(filename)
	return data

    if get_error:
        if get_cubic:
            if (os.path.exists(filename_cubic)):
	        cubic_exists = True
            else:
	        cubic_exists = False
                print "Warning: missing cubic logfile "+str(filename_cubic)
        else:
	    cubic_exists = False

        if (os.path.exists(filename_linear)):
	    linear_exists = True
        else:
	    linear_exists = False
            print "Warning: missing linear logfile "+str(filename_linear)

    data['size'] = num_units
    log = lf.Logfile(filename)
    data['logfile'] = log

    if get_error:
        if cubic_exists:
	    log_cubic = lf.Logfile(filename_cubic) 
            data['logfile_cubic'] = log_cubic
        if linear_exists:
            log_linear = lf.Logfile(filename_linear) 
            data['logfile_linear'] = log_linear

        if noise != '0':
            log_ref = lf.Logfile(filename_ref) 
	    data['logfile_ref'] = log_ref
	    data['noise'] = force_diff(log.log['posinp']['positions'], log_ref.log['posinp']['positions'], False)

    data['atoms'] = log.nat
    # calculate the number of occupied KS states (needed for band gap)
    nks = log.nat * 2
    data['energy'] = const.Ha2eV * (log.energy/log.nat)
    
    # error wrt cubic and/or linear scaling references
    if get_error:
        if cubic_exists:
            data['error'] = const.Ha2eV * abs(log.energy/log.nat - log_cubic.energy/log_cubic.nat)
            data['fmax_cubic'] = find_fmax(log_cubic.log['Atomic Forces (Ha/Bohr)'])
            data['forces_cubic'] = log_cubic.log['Atomic Forces (Ha/Bohr)']
            data['f_errors'] = force_diff(log.log['Atomic Forces (Ha/Bohr)'], log_cubic.log['Atomic Forces (Ha/Bohr)'], True)
        if linear_exists:
            data['error_linear'] = const.Ha2eV * abs(log.energy/log.nat - log_linear.energy/log_linear.nat)
            data['fmax_linear'] = find_fmax(log_linear.log['Atomic Forces (Ha/Bohr)'])
            data['forces_linear'] = log_linear.log['Atomic Forces (Ha/Bohr)']
            data['f_errors_linear'] = force_diff(log.log['Atomic Forces (Ha/Bohr)'], log_linear.log['Atomic Forces (Ha/Bohr)'], True)

    # other data which isn't used but which might be useful at some point - activate if needed
    #data['walltime'] = log.log['Walltime since initialization']
    if get_forces:
        data['fmax'] = find_fmax(log.log['Atomic Forces (Ha/Bohr)'])
        data['forces'] = log.log['Atomic Forces (Ha/Bohr)']

    # get atomic distances along NT (they should all be the same, so no need to get cubic separately)
    # NT must be oriented along z axis
    # allow for skipping this part, since for large systems the positions are not included in the logfile
    if get_pos:
        data['posinp'] = log.log['posinp']['positions']
        conversion = const.b2A
        if 'units' in log.log['posinp']:
            if units == 'angstroem':
                conversion = 1.0      

        # first need to find first point along z axis
        data['zmin'] = 1000000.0
        for pos in data['posinp']:
            if 'C' in pos:
                data['zmin'] = min(pos['C'][2], data['zmin'])
            elif 'Si' in pos:
                data['zmin'] = min(pos['Si'][2], data['zmin'])

        # the shift is needed for plotting densities
        data['zshift'] = log.log['Atomic structure']['Rigid Shift Applied (AU)'][2]

        # generate list of distances
        # also want the maximum tube length (useful for plotting)
        data['distances'] = list()
        data['dmax'] = 0.0
        for pos in data['posinp']:
            if 'C' in pos:
                data['distances'].append(conversion * (pos['C'][2] - data['zmin']))
            elif 'Si' in pos:
                data['distances'].append(conversion * (pos['Si'][2] - data['zmin']))
            if data['distances'][-1] > data['dmax']:
                data['dmax'] = data['distances'][-1]

    # extract information concerning the cost function
    data['wahba'] = {}
    if 'Maximum Wahba cost function value' in log.log['Input Hamiltonian']:
        # add the factor of 0.5 and convert from bohr^2 to angstrom^2
        wc = 0.5 * const.b2A**2

        # calculate these directly to get more dp's
    	#data['wahba']['max'] = log.log['Input Hamiltonian']['Maximum Wahba cost function value']
    	#data['wahba']['av'] = log.log['Input Hamiltonian']['Average Wahba cost function value']
        # read in all values of J, so that we can calculate the standard deviation and add error bars
        nj = 0
        data['wahba']['av'] = 0.0
        data['wahba']['max'] = 0.0
        for ft in log.log['Input Hamiltonian']['Fragment transformations']:
            j = wc * ft['Wahba cost function']
            data['wahba']['av'] += j
            data['wahba']['max'] = max(j, data['wahba']['max'])
            nj += 1
  
        data['wahba']['av'] /= nj
    
    	data['wahba']['std'] = 0.0
    	for ft in log.log['Input Hamiltonian']['Fragment transformations']:
            j = wc * ft['Wahba cost function']
            data['wahba']['std'] += (j - data['wahba']['av'])**2
    	
        data['wahba']['std'] = np.sqrt(data['wahba']['std'] / nj)

    else:
        # set the values to zero in the case where this isn't a fragment calculation
        data['wahba']['max'] = 0.0
        data['wahba']['av'] = 0.0
        data['wahba']['std'] = 0.0

    if (not get_dos):
        return data

    # extract data for the density of states
    npts=2500
    dos_log = log.get_dos(label=label, npts=npts)
    data['dos_energies'] = (np.array(dos_log.range))

    # hack to get fermi level for linear - in this case norb = norb_occ * 2
    # this should be generalized in future - i.e. we should make sure EFermi and the gap are accesible for both linear and cubic
    if (calc_type == 'cubic'):
        data['fermi'] = dos_log.ef
        if 'Complete list of energy eigenvalues' in log.log:
            evals = log.log['Complete list of energy eigenvalues']
            for e in evals:
                if (e.get("HOMO LUMO gap (AU, eV)")):
                    data['gap'] = e.get("HOMO LUMO gap (AU, eV)")[1]
        else:
            ev = nks - 1
            data['gap'] = const.Ha2eV * (log.evals[0][0][ev+1] - log.evals[0][0][ev])
    else:
        ev = nks - 1
        data['fermi'] = const.Ha2eV * log.evals[0][0][ev]
        data['gap'] = const.Ha2eV * (log.evals[0][0][ev+1] - log.evals[0][0][ev])

    data['dos'] = np.array([dos['dos'].curve(dos_log.range, sigma=smear)[1] for dos in dos_log.ens]).reshape(npts,1)
    
    return data

# routine for comparing either two sets of forces or positions
def force_diff(forces,forces_ref,funits):
    av_diff = 0.0
    max_diff = 0.0
    errors={}
    for f,fr in zip(forces,forces_ref):
        for xyz in range(3):
            if 'C' in f:
                av_diff = av_diff + abs(f['C'][xyz] - fr['C'][xyz])
                max_diff = max(max_diff, abs(f['C'][xyz] - fr['C'][xyz]))
            elif 'Si' in f:
                av_diff = av_diff + abs(f['Si'][xyz] - fr['Si'][xyz])
                max_diff = max(max_diff, abs(f['Si'][xyz] - fr['Si'][xyz]))

    av_diff = av_diff / (3.0 * len(forces))
    if funits:
    	errors['av'] = const.Hapb2eVpA * av_diff
    	errors['max'] = const.Hapb2eVpA * max_diff
    else:
    	errors['av'] = const.b2A * av_diff
    	errors['max'] = const.b2A * max_diff
    return errors

# routine for getting the maximum and average force components
def find_fmax(forces):
    fav = 0.0
    fmax = 0.0
    f={}
    for f in forces:
        for xyz in range(3):
            if 'C' in f:
                fav = fav + abs(f['C'][xyz])
                fmax = max(fmax, abs(f['C'][xyz]))
            elif 'Si' in f:
                fmax = max(fmax, abs(f['Si'][xyz]))
                fav = fav + abs(f['Si'][xyz])

    fav = fav / (3.0 * len(forces))
    f['av'] = const.Hapb2eVpA * fav
    f['max'] = const.Hapb2eVpA * fmax
    return f


# routine for reading in the onsite overlap matrix
def read_onsite_overlap(filename, nat_ring, num_units, nsf_c, nsf_si):

    # could generalize to work with 8 SFs/atom, but for now stick with the simplest case
    if nsf_c != 4 or nsf_si != 4:
        raise ValueError("Assuming 4 support functions per atom, unable to proceed!")

    f=open(filename,'r')

    oo={}
    oo['s']=list()
    oo['px']=list()
    oo['py']=list()
    oo['pz']=list()

    # don't need the following in this case, but might be useful in a more general case
    #num_sf = nat_ring * num_units * (nsf_c + nsf_si)
    #num_at = nat_ring * num_units * 2

    f3_old = -1
    f4_old = -1
    tag3 = 1
    tag4 = 1

    for l in f:
        field=l.split()
        # skip commented lines
        if field[0] == "#":
            continue

        # only read in if the SFs are of the same type, i.e. s and s (assume no internal rotation between px,y,z)
        # store atom numbers and overlap, don't need SF number
        if (field[4] == f4_old):
            tag4 = tag4 + 1
        else:
            tag4 = 1
        f4_old = field[4]

        if (field[1]=='1'):
           if (field[3] == f3_old):
              tag3 = tag3 + 1
           else:
              tag3 = 1
           f3_old = field[3]

        if (tag4 == 1 and tag3 == 1):
            oo['s'].append([field[3],field[4],field[2]])
        elif (tag4 == 2 and tag3 == 2):
            oo['px'].append([field[3],field[4],field[2]])
        elif (tag4 == 3 and tag3 == 3):
            oo['py'].append([field[3],field[4],field[2]])
        elif (tag4 == 4 and tag3 == 4):
            oo['pz'].append([field[3],field[4],field[2]])

    f.close()

    return oo



# manipulate onsite overlap for plotting 
# here we need to know the number of atoms in a unit
def process_onsite_overlap(nat_ring, natf, num_units, nsf_c, nsf_si, oo_full, ref_atom):

    # could generalize to work with 8 SFs/atom, but for now stick with the simple case
    if nsf_c != 4 or nsf_si != 4:
        raise ValueError("Assuming 4 support functions per atom, unable to proceed!")

    # this is the number of atoms we have to cycle through before we reach the next equivalent atom along the NT
    natr = nat_ring * 2

    # don't need the following in this case, but might be useful in a more general case
    #num_sf = nat_ring * num_units * (nsf_c + nsf_si)
    #num_at = nat_ring * num_units * 2

    oo = list()
    oo = {}
    oo['s'] = list()
    oo['px'] = list()
    oo['py'] = list()
    oo['pz'] = list()

    # we only want to compare atoms which are vertically aligned (i.e. no rotations)
    # so start with ref_atom and cycle over a full fragment
    for ra in range (ref_atom,ref_atom + natr):      
        for i, ooi in enumerate(oo_full['s']):    
       
            # if the first atom isn't the reference atom we're searching for, cycle
            if oo_full['s'][i][0] != str(ra):
                continue

            # check if the second atom is vertically aligned with the reference, if not cycle
            iat = int(oo_full['s'][i][1])
            if (iat - ra)%natr != 0:
                continue

            # work out which fragment number this is
            #ifrag = (iat - natf - 1 -(iat - natf - 1)%natf)/natf + 1
            ifrag = math.ceil(1.0 * iat / natf)

            # otherwise store the information
            oo['s'].append([iat, ifrag, float(oo_full['s'][i][2])])
            oo['px'].append([iat, ifrag, float(oo_full['px'][i][2])])
            oo['py'].append([iat, ifrag, float(oo_full['py'][i][2])])
            oo['pz'].append([iat, ifrag, float(oo_full['pz'][i][2])])

            # print some info for debugging
            #print 'ai, aj, ifrag, oo[s]', ra, iat, ifrag, oo_full['s'][i][2]

    return oo


# read in a density or a density difference from a text file
# in this case the units are atomic, not sure if this is always the case...
def read_density(filename, zshift):
    if not os.path.exists(filename):
        print filename+" not found"
        density = {}
        return density

    f=open(filename,'r')

    density = {}
    density['i'] = list()
    density['z'] = list()
    density['rho'] = list()
    density['rho_abs'] = list()

    for l in f:
        field=l.split()
        # skip commented lines
        if field[0] == "#":
            continue

        density['i'].append(int(field[0]))
        density['z'].append((float(field[1]) - zshift) * const.b2A)
        density['rho'].append(float(field[2])/(const.b2A**3))
        density['rho_abs'].append(abs(float(field[2])/(const.b2A**3)))

    f.close()

    return density



# function for making v_sim parameter file with atoms in the appropriate colours
def write_vsim_file(frag_types, max_frag_types, nfrag, colors, frag_col, periodic):   
    import elements
 
    if frag_col:
        if periodic:
            filename = "v_sim_"+str(frag_types*2)+"_"+str(nfrag)+"p.res"
        else:
            filename = "v_sim_"+str(frag_types*2)+"_"+str(nfrag)+".res"   
    else:
        filename = "v_sim_nf.res"

    print "writing to ",filename
    ff=open(filename,'w')

    ff.write("#V_Sim resources file v3.0\n")
    ff.write("#====================\n")

    ff.write("# Set the background of the background ; four floating point values (0. <= v <= 1.)\n")
    ff.write("backgroundColor_color:\n")
    ff.write("    1.000 1.000 1.000 1.000\n")
    ff.write("# Control if a box is drawn around the rendering area ; boolean (0 or 1)\n")
    ff.write("box_is_on:\n")
    ff.write("    0\n")
    ff.write("# Define the color of the box ; three floating point values (0. <= v <= 1.)\n")
    ff.write("box_color:\n")
    ff.write("    0.000 0.000 0.000\n")
    ff.write("# Define the width of the lines of the box ; one integer (1. <= v <= 10.)\n")
    ff.write("box_line_width:\n")
    ff.write("       2\n")
    ff.write("# Control if the legend is drawn ; boolean (0 or 1)\n")
    ff.write("legend_is_on:\n")
    ff.write("    0\n")
    ff.write("# Control if the axes are drawn ; boolean (0 or 1)\n")
    ff.write("axes_are_on:\n")
    ff.write("    0\n")

    
    # we might have to use incorrect element symbols to hack the fragment colouring
    ff.write("# The radius of the element and its shape, a real > 0. & [Sphere Cube Elipsoid Point]\n")
    if frag_col:
        for ifrag in range(1,nfrag+1):
            ff.write("atomic_radius_shape:\n")
            esi = elements.get_element(2*ifrag)
            ec = elements.get_element(2*ifrag-1)
            ff.write("  "+esi+" 0.650 Sphere\n")   
            ff.write("atomic_radius_shape:\n")
            ff.write("  "+ec+" 0.500 Sphere\n")   
    else:
        ff.write("atomic_radius_shape:\n")
        ff.write("   Si 0.650 Sphere\n")
        ff.write("atomic_radius_shape:\n")
        ff.write("    C 0.500 Sphere\n")

    
    ff.write("# Codes the main color in RedGreenBlueAlpha formatand the light effects on material, nine floats between 0. and 1.\n")
    if frag_col: 
        for ifrag in range(1,nfrag+1):
            if periodic and ifrag%2==0:
	        [fcol, ifr] = get_frag_colour(ifrag-1, frag_types, max_frag_types, nfrag, periodic)
            else:
	        [fcol, ifr] = get_frag_colour(ifrag, frag_types, max_frag_types, nfrag, periodic)

            #print ifrag, fcol - 2
            esi = elements.get_element(2*ifrag)
            ec = elements.get_element(2*ifrag-1)
            ff.write("element_color:\n")
            ff.write("    "+esi+" "+str(colors[fcol][0]/255.0)+" "\
                 +str(colors[fcol][1]/255.0)+" "\
                 +str(colors[fcol][2]/255.0)+" "\
                 +" 1.000   0.25 0.25 0.25 0.25 0.25\n")
            ff.write("element_color:\n")
            ff.write("    "+ec+" "+str(colors[fcol][0]/255.0)+" "\
                 +str(colors[fcol][1]/255.0)+" "\
                 +str(colors[fcol][2]/255.0)+" "\
                 +" 1.000   0.25 0.25 0.25 0.25 0.25\n")
    else:
        ff.write("element_color:\n")
        ff.write("   Si 0.000 0.685 0.137 1.000   0.20 0.54 0.69 0.50 0.20\n")
        ff.write("element_color:\n")
        ff.write("    C 0.411 0.411 0.411 1.000   0.20 0.54 0.69 0.50 0.20\n")
    
    
    ff.write("# This value is the width for all pairs drawn ; 0 < integer < 10\n")
    ff.write("pairWire_width:\n")
    ff.write("    2\n")
    ff.write("# Widths detail for each drawn link ; 0 < integer < 10\n")

    ff.write("# It chooses the colors of the cylinders according differents criterion ; 0 <= integer < 2\n")
    ff.write("cylinder_colorType:\n")
    ff.write("    1\n")
    ff.write("# This value is the default radius of the pairs drawn as cylinders ; 0 < real < 10\n")
    ff.write("pairCylinder_radius:\n")
    ff.write("    0.250000\n")
    ff.write("# This value is the radius for specific pairs drawn as cylinders ; element1 elemen2 0 < real < 10\n")

    ff.write("# Ask the opengl engine to draw pairs between elements ; boolean 0 or 1\n")
    ff.write("pairs_are_on:\n")
    ff.write("    1\n")
    ff.write("# Favorite method used to render files ; chain ('Wire pairs', 'Cylinder pairs')\n")
    ff.write("pairs_favoriteMethod:\n")
    ff.write("    Cylinder pairs\n")
    ff.write("# Draw a link between [ele1] [ele2] [0. <= dmin] [0. <= dmax]\n")
    ff.write("#                     [0. <= RGB <= 1.]x3 [bool: drawn] [bool: printLength] [string: method]\n")

    minp = "3.100"
    maxp = "3.600"
    if frag_col:
        for ifrag in range(1,nfrag+1):
            esi = elements.get_element(2*ifrag)
            ec = elements.get_element(2*ifrag-1)
            ff.write("pair_link:\n")
            ff.write("   "+esi+" "+ec+" "+str(minp)+" "+str(maxp)+"\n")
            ff.write("    1.000 0.600 0.200  1  0  Cylinder pairs\n")
            for jfrag in range(1,ifrag):
                esij = elements.get_element(2*jfrag)
                ecj = elements.get_element(2*jfrag-1)
                ff.write("pair_link:\n")
                ff.write("   "+esij+" "+ec+" "+str(minp)+" "+str(maxp)+"\n")
                ff.write("   0.500 0.500 0.500  1  0  Wire pairs\n")
                ff.write("pair_link:\n")
                ff.write("   "+ecj+" "+esi+" "+str(minp)+" "+str(maxp)+"\n")
                ff.write("   0.500 0.500 0.500  1  0  Wire pairs\n")
    else:
        ff.write("pair_link:\n")
        ff.write("    Si  C  "+str(minp)+" "+str(maxp)+"\n")
        ff.write("    1.000 0.600 0.200  1  0  Cylinder pairs\n")
    
    ff.write("# Define the colour of one surface ; 4 floats (RGBA) 5 floats (material)\n")
    ff.write("# Define some surface properties ; rendered (0 or 1) sensitive to planes (0 or 1)\n")
    ff.write("isosurface_color:\n")
    ff.write("   id=\"blue\" 0.000 0.094 0.725 0.769   0.20 1.00 0.50 0.50 0.00\n")
    ff.write("isosurface_properties:\n")
    ff.write("   id=\"blue\" 1 1\n")
    ff.write("isosurface_color:\n")
    ff.write("   id=\"red\" 0.725 0.094 0.000 0.769   0.20 1.00 0.50 0.50 0.00\n")
    ff.write("isosurface_properties:\n")
    ff.write("   id=\"red\" 1 1\n")

    ff.write("# Control if the fog is used ; boolean (0 or 1)\n")
    ff.write("fog_is_on:\n")
    ff.write("   1\n")
    ff.write("# Control if the fog uses a specific color ; boolean (0 or 1)\n")
    ff.write("fog_color_is_specific:\n")
    ff.write("   0\n")
    ff.write("# Define the color of the fog ; four floating point values (0. <= v <= 1.)\n")
    ff.write("fog_specific_color:\n")
    ff.write("    0.000 0.000 0.000 1.000\n")
    ff.write("# Define the position of the fog ; two floating point values (0. <= v <= 1.)\n")
    ff.write("fog_start_end:\n")
    ff.write("    0.400 0.700\n")

    ff.close()


# get the colour index for the fragment
# assumes that the colours array to be used has the right number of colours to be consistent with max_frag_types
def get_frag_colour(ifrag, frag_types, max_frag_types, total_frags, periodic):

    # get the fragment type for plotting colour
    fn = frag_types
    fmax = fn
    for f in range(1,fmax):
       	if ifrag <= f or ifrag > total_frags - f:
    	   fn = f
           break

    ifr = fn

    if ifrag%2 == 0:
        fn = fn + max_frag_types

    # to account for the fact that the array starts with cubic, linear then periodic frag
    if periodic:
	fn = fn + 1
    else:
	fn = fn + 2

    return [fn, ifr]

