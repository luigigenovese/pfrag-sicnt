#! /usr/bin/env python

# define some constants which will be useful for converting units

Ha2eV = 27.211396132            # Ha to eV
b2A = 0.529177249               # bohr to Angstrom
Hapb2eVpA = 51.42208619083232   # Ha/bohr to eV/Angstrom
