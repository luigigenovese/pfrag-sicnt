#V_Sim resources file v3.0
#====================
# Set the background of the background ; four floating point values (0. <= v <= 1.)
backgroundColor_color:
    1.000 1.000 1.000 1.000
# Control if a box is drawn around the rendering area ; boolean (0 or 1)
box_is_on:
    0
# Define the color of the box ; three floating point values (0. <= v <= 1.)
box_color:
    0.000 0.000 0.000
# Define the width of the lines of the box ; one integer (1. <= v <= 10.)
box_line_width:
       2
# Control if the legend is drawn ; boolean (0 or 1)
legend_is_on:
    0
# Control if the axes are drawn ; boolean (0 or 1)
axes_are_on:
    0
# The radius of the element and its shape, a real > 0. & [Sphere Cube Elipsoid Point]
atomic_radius_shape:
  He 0.650 Sphere
atomic_radius_shape:
  H 0.500 Sphere
atomic_radius_shape:
  Be 0.650 Sphere
atomic_radius_shape:
  Li 0.500 Sphere
atomic_radius_shape:
  C 0.650 Sphere
atomic_radius_shape:
  B 0.500 Sphere
atomic_radius_shape:
  O 0.650 Sphere
atomic_radius_shape:
  N 0.500 Sphere
atomic_radius_shape:
  Ne 0.650 Sphere
atomic_radius_shape:
  F 0.500 Sphere
atomic_radius_shape:
  Mg 0.650 Sphere
atomic_radius_shape:
  Na 0.500 Sphere
atomic_radius_shape:
  Si 0.650 Sphere
atomic_radius_shape:
  Al 0.500 Sphere
atomic_radius_shape:
  S 0.650 Sphere
atomic_radius_shape:
  P 0.500 Sphere
atomic_radius_shape:
  Ar 0.650 Sphere
atomic_radius_shape:
  Cl 0.500 Sphere
atomic_radius_shape:
  Ca 0.650 Sphere
atomic_radius_shape:
  K 0.500 Sphere
# Codes the main color in RedGreenBlueAlpha formatand the light effects on material, nine floats between 0. and 1.
element_color:
    He 0.788235294118 0.0 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    H 0.788235294118 0.0 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Be 1.0 0.0 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Li 1.0 0.0 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    C 0.309803921569 0.576470588235 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    B 0.309803921569 0.576470588235 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    O 0.513725490196 0.788235294118 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    N 0.513725490196 0.788235294118 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Ne 0.576470588235 0.0 0.576470588235  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    F 0.576470588235 0.0 0.576470588235  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Mg 0.0 0.18431372549 1.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Na 0.0 0.18431372549 1.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Si 0.211764705882 0.396078431373 1.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Al 0.211764705882 0.396078431373 1.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    S 0.788235294118 0.407843137255 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    P 0.788235294118 0.407843137255 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Ar 1.0 0.623529411765 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Cl 1.0 0.623529411765 0.0  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    Ca 0.156862745098 0.274509803922 0.588235294118  1.000   0.25 0.25 0.25 0.25 0.25
element_color:
    K 0.156862745098 0.274509803922 0.588235294118  1.000   0.25 0.25 0.25 0.25 0.25
# This value is the width for all pairs drawn ; 0 < integer < 10
pairWire_width:
    2
# Widths detail for each drawn link ; 0 < integer < 10
# It chooses the colors of the cylinders according differents criterion ; 0 <= integer < 2
cylinder_colorType:
    1
# This value is the default radius of the pairs drawn as cylinders ; 0 < real < 10
pairCylinder_radius:
    0.250000
# This value is the radius for specific pairs drawn as cylinders ; element1 elemen2 0 < real < 10
# Ask the opengl engine to draw pairs between elements ; boolean 0 or 1
pairs_are_on:
    1
# Favorite method used to render files ; chain ('Wire pairs', 'Cylinder pairs')
pairs_favoriteMethod:
    Cylinder pairs
# Draw a link between [ele1] [ele2] [0. <= dmin] [0. <= dmax]
#                     [0. <= RGB <= 1.]x3 [bool: drawn] [bool: printLength] [string: method]
pair_link:
   He H 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   Be Li 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He Li 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H Be 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C B 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He B 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H C 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be B 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li C 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   O N 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He N 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H O 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be N 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li O 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C N 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   B O 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ne F 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He F 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H Ne 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be F 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li Ne 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C F 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   B Ne 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   O F 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   N Ne 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Mg Na 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He Na 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H Mg 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be Na 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li Mg 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C Na 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   B Mg 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   O Na 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   N Mg 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ne Na 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   F Mg 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Si Al 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He Al 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H Si 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be Al 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li Si 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C Al 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   B Si 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   O Al 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   N Si 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ne Al 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   F Si 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Mg Al 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Na Si 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   S P 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He P 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H S 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be P 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li S 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C P 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   B S 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   O P 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   N S 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ne P 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   F S 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Mg P 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Na S 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Si P 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Al S 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ar Cl 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   B Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   O Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   N Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ne Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   F Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Mg Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Na Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Si Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Al Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   S Cl 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   P Ar 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ca K 3.100 3.600
    1.000 0.600 0.200  1  0  Cylinder pairs
pair_link:
   He K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   H Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Be K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Li Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   C K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   B Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   O K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   N Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ne K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   F Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Mg K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Na Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Si K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Al Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   S K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   P Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Ar K 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
pair_link:
   Cl Ca 3.100 3.600
   0.500 0.500 0.500  1  0  Wire pairs
# Define the colour of one surface ; 4 floats (RGBA) 5 floats (material)
# Define some surface properties ; rendered (0 or 1) sensitive to planes (0 or 1)
isosurface_color:
   id="blue" 0.000 0.094 0.725 0.769   0.20 1.00 0.50 0.50 0.00
isosurface_properties:
   id="blue" 1 1
isosurface_color:
   id="red" 0.725 0.094 0.000 0.769   0.20 1.00 0.50 0.50 0.00
isosurface_properties:
   id="red" 1 1
# Control if the fog is used ; boolean (0 or 1)
fog_is_on:
   1
# Control if the fog uses a specific color ; boolean (0 or 1)
fog_color_is_specific:
   0
# Define the color of the fog ; four floating point values (0. <= v <= 1.)
fog_specific_color:
    0.000 0.000 0.000 1.000
# Define the position of the fog ; two floating point values (0. <= v <= 1.)
fog_start_end:
    0.400 0.700
